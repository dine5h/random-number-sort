# Random Number Sorting Application

Application home page : http://localhost:8000

## Purpose

This application sorts the list of random numbers entered by user. After sorting, it displays the sorted numbers and position changes involved in sorting

## Usage

![Application image](images/usage.PNG)

1. Enter comma separated random numbers in the input field
2. Click on Sort button to view the results
3. Click on navigation buttons (<< and >>) to view the saved values

## Technologies Used

* Java 1.8
* Spring Boot 2.1.4.RELEASE
* Vaadin Flow 13.0.3 (for UI) - Vaadin framework has been chosen for simplicity of having both UI and service in one codebase
* H2 database

## Algorithm used

As the number range to be sorted is smaller and position changes need to be tracked, **Counting sort algorithm** is applied. The occurence count of each number within the range is taken and sorted list is prepared based on this count.

## Execution

* Run command `mvn spring-boot:run` to start the application and it can be accessed on http://localhost:8000
* Run command `mvn test` to execute the integration/functional tests
