package dinesh.code.randomnumbersort;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import dinesh.code.randomnumbersort.dto.NumbersDTO;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SortControllerTests {

	// URLs
	public static final String SORT_URL = "/sort";
	public static final String FETCH_URL = "/fetch";
	
	// Info messages
	public static final String NON_NUMERIC_ENTRY = "Please remove non-numeric entry from list";
	public static final String ID_NOT_FOUND = "No further values saved";
	
	@Autowired
    private MockMvc mvc;
	
	@Test
	public void testSortSuccess() throws JsonProcessingException, Exception {
		NumbersDTO numbersDTO = new NumbersDTO();
		numbersDTO.setInputString("5,4,3,2,1");

		ObjectWriter objectWriter = new ObjectMapper().writer();
		
		mvc.perform(post(SORT_URL)
			      .contentType(MediaType.APPLICATION_JSON)
			      .content(objectWriter.writeValueAsBytes(numbersDTO)))
			      .andExpect(status().isOk())
			      .andExpect(jsonPath("$.status").value(true))
			      .andExpect(jsonPath("$.sortedList[0]").value(1))
			      .andExpect(jsonPath("$.sortedList[1]").value(2))
			      .andExpect(jsonPath("$.sortedList[2]").value(3))
			      .andExpect(jsonPath("$.sortedList[3]").value(4))
			      .andExpect(jsonPath("$.sortedList[4]").value(5))
			      .andExpect(jsonPath("$.positionChangeList[0]").value(4))
			      .andExpect(jsonPath("$.positionChangeList[1]").value(2))
			      .andExpect(jsonPath("$.positionChangeList[2]").value(0))
			      .andExpect(jsonPath("$.positionChangeList[3]").value(-2))
			      .andExpect(jsonPath("$.positionChangeList[4]").value(-4));
	}

	@Test
	public void testSortBadRequest() throws JsonProcessingException, Exception {
		NumbersDTO numbersDTO = new NumbersDTO();
		numbersDTO.setInputString("");

		ObjectWriter objectWriter = new ObjectMapper().writer();
		
		mvc.perform(post(SORT_URL)
			      .contentType(MediaType.APPLICATION_JSON)
			      .content(objectWriter.writeValueAsBytes(numbersDTO)))
			      .andExpect(status().isBadRequest());
	}
	
	@Test
	public void testSortDuplicateValues() throws JsonProcessingException, Exception {
		NumbersDTO numbersDTO = new NumbersDTO();
		numbersDTO.setInputString("1,2,3,2,1");

		ObjectWriter objectWriter = new ObjectMapper().writer();
		
		mvc.perform(post(SORT_URL)
			      .contentType(MediaType.APPLICATION_JSON)
			      .content(objectWriter.writeValueAsBytes(numbersDTO)))
			      .andExpect(status().isOk())
			      .andExpect(jsonPath("$.status").value(true))
			      .andExpect(jsonPath("$.sortedList[0]").value(1))
			      .andExpect(jsonPath("$.sortedList[1]").value(1))
			      .andExpect(jsonPath("$.sortedList[2]").value(2))
			      .andExpect(jsonPath("$.sortedList[3]").value(2))
			      .andExpect(jsonPath("$.sortedList[4]").value(3))
			      .andExpect(jsonPath("$.positionChangeList[0]").value(0))
			      .andExpect(jsonPath("$.positionChangeList[1]").value(3))
			      .andExpect(jsonPath("$.positionChangeList[2]").value(-1))
			      .andExpect(jsonPath("$.positionChangeList[3]").value(0))
			      .andExpect(jsonPath("$.positionChangeList[4]").value(-2));
	}
	
	@Test
	public void testSortNonNumeric() throws JsonProcessingException, Exception {
		NumbersDTO numbersDTO = new NumbersDTO();
		numbersDTO.setInputString("1,2,A,2,1");

		ObjectWriter objectWriter = new ObjectMapper().writer();
		
		mvc.perform(post(SORT_URL)
			      .contentType(MediaType.APPLICATION_JSON)
			      .content(objectWriter.writeValueAsBytes(numbersDTO)))
			      .andExpect(status().isOk())
			      .andExpect(jsonPath("$.message").value(NON_NUMERIC_ENTRY));
	}
	
	@Test
	public void testFetchSuccess()throws JsonProcessingException, Exception {
		NumbersDTO numbersDTO = new NumbersDTO();
		numbersDTO.setInputString("5,4,3,2,1");

		ObjectMapper objectMapper = new ObjectMapper();
		
		ResultActions resultActions = mvc.perform(post(SORT_URL)
			      .contentType(MediaType.APPLICATION_JSON)
			      .content(objectMapper.writeValueAsBytes(numbersDTO)))
			      .andExpect(status().isOk())
			      .andExpect(jsonPath("$.status").value(true))
			      .andExpect(jsonPath("$.id").isNumber());
		
		MvcResult result = resultActions.andReturn();
		String contentAsString = result.getResponse().getContentAsString();
		NumbersDTO numbersDTOResponse = objectMapper.readValue(contentAsString, NumbersDTO.class);
		
		mvc.perform(post(FETCH_URL)
			      .param("id", numbersDTOResponse.getId().toString()))
			      .andExpect(status().isOk())
			      .andExpect(jsonPath("$.status").value(true))
			      .andExpect(jsonPath("$.inputString").value("5,4,3,2,1"));
	}
	
	@Test
	public void testFetchBadRequest()throws JsonProcessingException, Exception {
		NumbersDTO numbersDTO = new NumbersDTO();
		numbersDTO.setInputString("5,4,3,2,1");

		mvc.perform(post(FETCH_URL))
			      .andExpect(status().isBadRequest());
	}
	
	@Test
	public void testFetchNonExisting()throws JsonProcessingException, Exception {
		NumbersDTO numbersDTO = new NumbersDTO();
		numbersDTO.setInputString("5,4,3,2,1");

		mvc.perform(post(FETCH_URL)
			      .param("id", "10"))
			      .andExpect(status().isOk())
			      .andExpect(jsonPath("$.status").value(false))
			      .andExpect(jsonPath("$.message").value(ID_NOT_FOUND));
	}
}
