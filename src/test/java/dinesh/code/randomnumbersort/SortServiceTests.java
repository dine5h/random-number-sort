package dinesh.code.randomnumbersort;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import dinesh.code.randomnumbersort.dto.NumbersDTO;
import dinesh.code.randomnumbersort.entity.RandomNumberEntity;
import dinesh.code.randomnumbersort.exception.SortException;
import dinesh.code.randomnumbersort.repo.RandomNumberRepo;
import dinesh.code.randomnumbersort.service.SortService;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class SortServiceTests {

	@Mock
	RandomNumberRepo randomNumberRepo;
	
	@InjectMocks
	@Autowired
	SortService sortService;
	
	@Test
	public void testSplitAndParsePositive() throws SortException
	{
		String inputString = "5, 4, 3, 2, 1";
		String separator = ",";
		List<Integer> expectedList = Arrays.asList(5,4,3,2,1);
		
		Assert.assertTrue(expectedList.equals(sortService.splitAndParse(inputString, separator)));
	}
	
	@Test
	public void testSplitAndParseNegative() throws SortException
	{
		String inputString = "5, 4, X, 2, 1";
		String separator = ",";
		
		try
		{
			sortService.splitAndParse(inputString, separator);
		}
		catch(SortException e)
		{
			Assert.assertEquals("Please remove non-numeric entry from list", e.getMessage());
		}
	}
	
	@Test
	public void testCountingSortAlgo()
	{
		List<Integer> inputList = Arrays.asList(5,4,3,2,1);
		NumbersDTO numbersDTO = sortService.countingSortAlgo(inputList);
		
		List<Integer> sortedList = Arrays.asList(1,2,3,4,5);
		List<Integer> positionChange = Arrays.asList(4,2,0,-2,-4);
		
		Assert.assertTrue(sortedList.equals(numbersDTO.getSortedList()));
		Assert.assertTrue(positionChange.equals(numbersDTO.getPositionChangeList()));
	}
	
	@Test
	public void testCalcCumulativeCount()
	{
		Map<Integer,Integer> countMap = new HashMap<>();
		countMap.put(1, 1);
		countMap.put(2, 1);
		countMap.put(3, 2);
		countMap.put(4, 1);
		countMap.put(5, 1);
		
		Map<Integer,Integer> expectedMap = new HashMap<>();
		expectedMap.put(1, 0);
		expectedMap.put(2, 1);
		expectedMap.put(3, 2);
		expectedMap.put(4, 4);
		expectedMap.put(5, 5);
		
		Map<Integer,Integer> actualMap = sortService.calcCumulativeCount(countMap,1,5);
		Assert.assertTrue(expectedMap.equals(actualMap));
	}
	
	@Test
	public void testFetchSavedNumberPositive() throws SortException
	{
		String inputString = "5,4,3,2,1";
		Integer id = 1;

		List<Integer> sortedList = Arrays.asList(1,2,3,4,5);
		List<Integer> positionChange = Arrays.asList(4,2,0,-2,-4);

		RandomNumberEntity randomNumberEntity = new RandomNumberEntity();
		randomNumberEntity.setId(id);
		randomNumberEntity.setNumberList(inputString);
		Mockito.when(randomNumberRepo.findById(id)).thenReturn(Optional.of(randomNumberEntity));
		
		NumbersDTO outputNumbersDTO = sortService.fetchSavedNumber(id);
		
		Assert.assertTrue(id.equals(outputNumbersDTO.getId()));
		Assert.assertTrue(inputString.equals(outputNumbersDTO.getInputString()));
		Assert.assertTrue(sortedList.equals(outputNumbersDTO.getSortedList()));
		Assert.assertTrue(positionChange.equals(outputNumbersDTO.getPositionChangeList()));
	}
	
	@Test
	public void testFetchSavedNumberNegative()
	{
		try
		{
			sortService.fetchSavedNumber(99);
		}
		catch(SortException e)
		{
			Assert.assertEquals("No further values saved", e.getMessage());
		}
	}
}