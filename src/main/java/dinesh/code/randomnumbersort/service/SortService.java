package dinesh.code.randomnumbersort.service;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dinesh.code.randomnumbersort.dto.NumbersDTO;
import dinesh.code.randomnumbersort.entity.RandomNumberEntity;
import dinesh.code.randomnumbersort.exception.SortException;
import dinesh.code.randomnumbersort.repo.RandomNumberRepo;

@Service
public class SortService {

	// Success messages
	public static final String SORT_SUCCESS = "Given numbers have been sorted successfully";
	public static final String FETCH_SUCCESS = "Saved numbers have been fetched successfully";
	
	// Failure messages
	public static final String SORT_FAILURE = "Error occured while sorting given numbers";
	public static final String FETCH_FAILURE = "Error occured while fetchng saved numbers";
	
	// Info messages
	public static final String BLANK_ENTRY = "Please remove blank entry from list";
	public static final String NON_NUMERIC_ENTRY = "Please remove non-numeric entry from list";
	public static final String OUT_OF_RANGE_ENTRY = "Please remove out of range entry from list";
	public static final String ID_NOT_FOUND = "No further values saved";
	
	// Other constants
	public static final String LIST_SEPARATOR = ",";
	public static final int NUMBER_MIN = 0;
	public static final int NUMBER_MAX = 100;
	public static final int LIST_LIMIT = 50;
	public static final String NUMBER_REGEX = "^[0-9]+(,[0-9]+)*$";
	
	@Autowired
	RandomNumberRepo randomNumberRepo;
	
	//Wrapper method to sort given list of numbers
	@Transactional
	public NumbersDTO sort(NumbersDTO inputNumbersDTO) throws SortException
	{
		List<Integer> unsortedList = splitAndParse(inputNumbersDTO.getInputString(),LIST_SEPARATOR);

		NumbersDTO numbersDTO = countingSortAlgo(unsortedList);
		numbersDTO.setId(saveRandomNumber(numbersDTO.getUnsortedList()));
		numbersDTO.setInputString(inputNumbersDTO.getInputString());
		
		return numbersDTO;
	}
	
	//Split string containing numbers into list of numbers
	public List<Integer> splitAndParse(String inputString, String separator) throws SortException
	{
		try
		{
			 return Stream.of(inputString.split(separator))
					.parallel()
					.limit(LIST_LIMIT)
					.map(String::trim)
					.map(Integer::parseInt)
					.filter(number -> number >= NUMBER_MIN && number <= NUMBER_MAX)
					.collect(Collectors.toList());
		}
		catch(NumberFormatException | NullPointerException e)
		{
			throw new SortException(NON_NUMERIC_ENTRY, e);
		}
	}

	//Counting sort algorithm implementation
	// Takes unsortedList
	// Returns sortedList, positionChangeList and timeTakenMillis
	public NumbersDTO countingSortAlgo(List<Integer> unsortedList) {
		// Start time 
		Instant start = Instant.now();
		
		// Map to count the occurrences of numbers within range
		// Number will be the key and count will be the value
		Map<Integer,Integer> numberCountMap = new HashMap<>();
		unsortedList.forEach(number -> numberCountMap.merge(number, 1, Integer::sum));
		
		// Preparing cumulative count array - starts with zero
		Map<Integer,Integer> cumulativeCountMap = calcCumulativeCount(numberCountMap,NUMBER_MIN,NUMBER_MAX);
		
		Integer listSize = unsortedList.size();
		List<Integer> sortedList = new ArrayList<>(unsortedList);
		List<Integer> positionChangeList = new ArrayList<>(unsortedList);
		
		// Preparing sorted list based on cumulative count
		IntStream.range(0, listSize).forEach(index -> {
			Integer number = unsortedList.get(index);
			Integer newIndex = cumulativeCountMap.get(number);
			sortedList.set(newIndex, number);
			positionChangeList.set(newIndex, index - newIndex);
			// Occurrence count and Cumulative count are modified to track repeating numbers
			numberCountMap.put(number, numberCountMap.get(number) - 1);
			if(numberCountMap.get(number) > 0)
				cumulativeCountMap.put(number, cumulativeCountMap.get(number) + 1);
		});

		NumbersDTO numbersDTO = new NumbersDTO();
		numbersDTO.setSortedList(sortedList);
		numbersDTO.setUnsortedList(unsortedList);
		numbersDTO.setPositionChangeList(positionChangeList);
		numbersDTO.setTimeTakenMillis(Duration.between(start, Instant.now()).toMillis());
		return numbersDTO;
	}
	
	
	//Method to convert count list to cumulative count list 
	public Map<Integer,Integer> calcCumulativeCount(Map<Integer,Integer> numberCountMap,Integer numberMin,Integer numberMax)
	{
		Map<Integer,Integer> cumulativeCountMap = new HashMap<>();
		cumulativeCountMap.put(numberMin, 0);
		IntStream.rangeClosed(numberMin + 1, numberMax)
				.forEach(number -> {
					Integer cumulativeCount = cumulativeCountMap.get(number - 1);
					if(numberCountMap.containsKey(number - 1))
					{
						cumulativeCount = cumulativeCount + numberCountMap.get(number - 1);
					}
					cumulativeCountMap.put(number, cumulativeCount);
				});
		return cumulativeCountMap;
	}
	
	//Method to persist random number list
	@Transactional
	public Integer saveRandomNumber(List<Integer> numberList)
	{
		String listString = numberList.stream().map(num -> num.toString()).collect(Collectors.joining(","));
		RandomNumberEntity randomNumberEntity = new RandomNumberEntity();
		randomNumberEntity.setNumberList(listString);
		randomNumberEntity = randomNumberRepo.save(randomNumberEntity);
		return randomNumberEntity.getId();
	}

	//Service to fetch saved numbers
	@Transactional
	public NumbersDTO fetchSavedNumber(Integer id) throws SortException{
		Optional<RandomNumberEntity> randomNumberEntity = randomNumberRepo.findById(id);
		NumbersDTO numbersDTO = new NumbersDTO();
		if(randomNumberEntity.isPresent())
		{
			List<Integer> unsortedList = splitAndParse(randomNumberEntity.get().getNumberList(),LIST_SEPARATOR);
			numbersDTO = countingSortAlgo(unsortedList);
			numbersDTO.setId(id);
			numbersDTO.setInputString(randomNumberEntity.get().getNumberList());
		}
		else
		{
			throw new SortException(ID_NOT_FOUND);
		}
		return numbersDTO;
	}

}
