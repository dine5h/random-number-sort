package dinesh.code.randomnumbersort.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dinesh.code.randomnumbersort.dto.NumbersDTO;
import dinesh.code.randomnumbersort.exception.SortException;
import dinesh.code.randomnumbersort.service.SortService;

@RestController
public class SortController {

	// URLs
	public static final String SORT_URL = "/sort";
	public static final String FETCH_URL = "/fetch";

	// Success messages
	public static final String SORT_SUCCESS = "Given numbers have been sorted successfully";
	public static final String FETCH_SUCCESS = "Saved numbers have been fetched successfully";

	@Autowired
	SortService sortService;

	/**
	 * Endpoint to sort list of given random numbers
	 * 
	 * @param inputNumbersDTO
	 * @return outputNumbersDTO
	 */
	@PostMapping(SORT_URL)
	public NumbersDTO sort(@Valid @RequestBody NumbersDTO inputNumbersDTO) {
		NumbersDTO outputNumbersDTO = new NumbersDTO();
		try {
			outputNumbersDTO = sortService.sort(inputNumbersDTO);
			outputNumbersDTO.setStatus(true);
			outputNumbersDTO.setMessage(SORT_SUCCESS);
		} catch (SortException e) {
			outputNumbersDTO.setStatus(false);
			outputNumbersDTO.setMessage(e.getMessage());
		}
		return outputNumbersDTO;
	}

	/**
	 * Endpoint to fetch saved numbers list
	 * 
	 * @param id
	 * @return
	 */
	@PostMapping(FETCH_URL)
	public NumbersDTO fetch(@RequestParam Integer id) {
		NumbersDTO outputNumbersDTO = new NumbersDTO();
		try {
			outputNumbersDTO = sortService.fetchSavedNumber(id);
			outputNumbersDTO.setStatus(true);
			outputNumbersDTO.setMessage(FETCH_SUCCESS);
		} catch (SortException e) {
			outputNumbersDTO.setStatus(false);
			outputNumbersDTO.setMessage(e.getMessage());
		}
		return outputNumbersDTO;
	}
}
