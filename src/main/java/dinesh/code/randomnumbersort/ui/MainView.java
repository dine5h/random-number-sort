package dinesh.code.randomnumbersort.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.ColumnTextAlign;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;

import dinesh.code.randomnumbersort.controller.SortController;
import dinesh.code.randomnumbersort.dto.NumbersDTO;
import dinesh.code.randomnumbersort.dto.SortGridDTO;

@Route
public class MainView extends VerticalLayout {

	public static final String TITLE_LABEL = "<h3>Random Number Sort Application</h3>";
	public static final String INPUT_TEXTFIELD = "Enter comma separated random numbers :";
	public static final String PREVIOUS_BUTTON = "<<";
	public static final String NEXT_BUTTON = ">>";
	public static final String SORT_BUTTON = "Sort";
	public static final String TIME_TAKEN_LABEL = "Time Taken : ";
	public static final String TIME_UNIT_TEXT = " milliseconds";
	public static final String INDEX_TEXT = "Index";
	public static final String BEFORE_SORT_TEXT = "Before Sort";
	public static final String AFTER_SORT_TEXT = "After Sort";
	public static final String POSITION_CHANGE_TEXT = "Position Change";
	public static final String GRID_WIDTH = "50%";
	public static final int NOTIFICATION_DURATION = 2000;
	public static final String INPUT_TEXTFIELD_WIDTH = "75%";
	public static final String NUMBER_REGEX = "^[0-9]+(,[0-9]+)*$";
	public static final int LIST_LIMIT = 50;
	public static final String INPUT_FIELD_EMPTY_MSG = "Please enter the input";

	@Autowired
	SortController sortController;

	private static final long serialVersionUID = 1L;

	private TextField inputTextField = new TextField(INPUT_TEXTFIELD);
	private Label timeTakenLabel = new Label(TIME_TAKEN_LABEL);
	private Grid<SortGridDTO> grid = new Grid<>();
	private int currentId;

	// Setting up UI layout
	public MainView() {

		Label titleLabel = new Label();
		titleLabel.getElement().setProperty("innerHTML", TITLE_LABEL);
		titleLabel.setSizeUndefined();
		this.add(titleLabel);

		inputTextField.setSizeUndefined();
		inputTextField.setMaxLength(LIST_LIMIT * 4);
		inputTextField.setWidth(INPUT_TEXTFIELD_WIDTH);
		inputTextField.addValueChangeListener(event -> {
			if (!event.getValue().matches(NUMBER_REGEX))
				inputTextField.setValue(
						event.getValue().replaceAll("[^0-9,]", "").replaceAll(",+", ",").replaceAll("^,", ""));
		});
		inputTextField.setValueChangeMode(ValueChangeMode.EAGER);
		this.add(inputTextField);

		HorizontalLayout horizontalLayout = new HorizontalLayout();
		Button previousButton = new Button(PREVIOUS_BUTTON, e -> fetchClicked(this.currentId - 1));
		Button nextButton = new Button(NEXT_BUTTON, e -> fetchClicked(this.currentId + 1));
		Button sortButton = new Button(SORT_BUTTON, e -> sortClicked());
		horizontalLayout.add(previousButton);
		horizontalLayout.add(sortButton);
		horizontalLayout.add(nextButton);
		this.add(horizontalLayout);

		timeTakenLabel.setVisible(false);
		this.add(timeTakenLabel);

		HorizontalLayout gridLayout = new HorizontalLayout();
		gridLayout.setWidth(GRID_WIDTH);
		grid.addColumn(SortGridDTO::getUnsortedNumber).setHeader(BEFORE_SORT_TEXT).setTextAlign(ColumnTextAlign.CENTER);
		grid.addColumn(SortGridDTO::getSortedNumber).setHeader(AFTER_SORT_TEXT).setTextAlign(ColumnTextAlign.CENTER);
		grid.addColumn(SortGridDTO::getPositionChange).setHeader(POSITION_CHANGE_TEXT)
				.setTextAlign(ColumnTextAlign.CENTER);
		grid.setVisible(false);
		gridLayout.add(grid);
		this.add(gridLayout);

		this.setAlignItems(Alignment.CENTER);
	}

	// Sort button action
	private void sortClicked() {
		String inputString = inputTextField.getValue();
		String message = "";
		
		if(inputString != null && inputString.length() > 0)
		{
			NumbersDTO inputNumbersDTO = new NumbersDTO();
			inputNumbersDTO.setInputString(inputString);
			NumbersDTO numbersDTO = sortController.sort(inputNumbersDTO);
			if (numbersDTO.getStatus()) {
				setGridDataAndTimeTaken(numbersDTO);
				this.currentId = numbersDTO.getId();
			}
			message = numbersDTO.getMessage();
		}
		else
		{
			message = INPUT_FIELD_EMPTY_MSG;
		}

		Notification.show(message, NOTIFICATION_DURATION, Position.MIDDLE);
	}

	// Navigate button action
	private void fetchClicked(Integer id) {
		NumbersDTO numbersDTO = sortController.fetch(id);

		if (numbersDTO.getStatus()) {
			setGridDataAndTimeTaken(numbersDTO);
			this.currentId = numbersDTO.getId();
		}

		Notification.show(numbersDTO.getMessage(), NOTIFICATION_DURATION, Position.MIDDLE);
	}

	// Method to populate grid data
	private void setGridDataAndTimeTaken(NumbersDTO numbersDTO) {
		Integer columnCount = numbersDTO.getUnsortedList().size();

		List<SortGridDTO> gridDataList = new ArrayList<>();
		IntStream.range(0, columnCount)
				.forEach(index -> gridDataList.add(new SortGridDTO(numbersDTO.getUnsortedList().get(index),
						numbersDTO.getSortedList().get(index), numbersDTO.getPositionChangeList().get(index))));
		grid.setItems(gridDataList);
		grid.setVisible(true);

		timeTakenLabel.setText(TIME_TAKEN_LABEL + numbersDTO.getTimeTakenMillis() + TIME_UNIT_TEXT);
		timeTakenLabel.setVisible(true);

		inputTextField.setValue(numbersDTO.getInputString() != null ? numbersDTO.getInputString() : "");
	}
}