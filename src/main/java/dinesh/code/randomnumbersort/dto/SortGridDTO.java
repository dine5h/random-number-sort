package dinesh.code.randomnumbersort.dto;

public class SortGridDTO {
	private Integer unsortedNumber;
	private Integer sortedNumber;
	private Integer positionChange;

	public SortGridDTO(Integer unsortedNumber, Integer sortedNumber, Integer positionChange) {
		super();
		this.unsortedNumber = unsortedNumber;
		this.sortedNumber = sortedNumber;
		this.positionChange = positionChange;
	}

	public Integer getUnsortedNumber() {
		return unsortedNumber;
	}

	public void setUnsortedNumber(Integer unsortedNumber) {
		this.unsortedNumber = unsortedNumber;
	}

	public Integer getSortedNumber() {
		return sortedNumber;
	}

	public void setSortedNumber(Integer sortedNumber) {
		this.sortedNumber = sortedNumber;
	}

	public Integer getPositionChange() {
		return positionChange;
	}

	public void setPositionChange(Integer positionChange) {
		this.positionChange = positionChange;
	}
}