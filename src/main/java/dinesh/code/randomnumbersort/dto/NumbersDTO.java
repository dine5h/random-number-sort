package dinesh.code.randomnumbersort.dto;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class NumbersDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	@NotNull
	@NotBlank
	private String inputString;
	private List<Integer> unsortedList;
	private List<Integer> sortedList;
	private List<Integer> positionChangeList;
	private Long timeTakenMillis;
	private Boolean status;
	private String message;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getInputString() {
		return inputString;
	}

	public void setInputString(String inputString) {
		this.inputString = inputString;
	}

	public List<Integer> getUnsortedList() {
		return unsortedList;
	}

	public void setUnsortedList(List<Integer> unsortedList) {
		this.unsortedList = unsortedList;
	}

	public List<Integer> getSortedList() {
		return sortedList;
	}

	public void setSortedList(List<Integer> sortedList) {
		this.sortedList = sortedList;
	}

	public List<Integer> getPositionChangeList() {
		return positionChangeList;
	}

	public void setPositionChangeList(List<Integer> positionChangeList) {
		this.positionChangeList = positionChangeList;
	}

	public Long getTimeTakenMillis() {
		return timeTakenMillis;
	}

	public void setTimeTakenMillis(Long timeTakenMillis) {
		this.timeTakenMillis = timeTakenMillis;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
