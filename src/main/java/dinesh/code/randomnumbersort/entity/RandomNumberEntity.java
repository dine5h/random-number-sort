package dinesh.code.randomnumbersort.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "random_number")
public class RandomNumberEntity {

	@Id
	@GeneratedValue
	@Column
	private Integer id;

	@Column(name = "number_list")
	private String numberList;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNumberList() {
		return numberList;
	}

	public void setNumberList(String numberList) {
		this.numberList = numberList;
	}
}
