package dinesh.code.randomnumbersort;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RandomNumberSortApplication {

	public static void main(String[] args) {
		SpringApplication.run(RandomNumberSortApplication.class, args);
	}

}
