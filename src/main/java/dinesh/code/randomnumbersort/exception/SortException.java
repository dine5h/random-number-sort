package dinesh.code.randomnumbersort.exception;

public class SortException extends Exception {

	private static final long serialVersionUID = 1L;

	private String message;

	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 * @param cause
	 */
	public SortException(String message, Throwable cause) {
		super(message, cause);
		this.message = message;
	}

	/**
	 * @param message
	 */
	public SortException(String message) {
		super(message);
		this.message = message;
	}

	/**
	 * @param cause
	 */
	public SortException(Throwable cause) {
		super(cause);
		this.message = cause.getMessage();
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SortException [message=").append(message).append("]");
		return builder.toString();
	}
}
