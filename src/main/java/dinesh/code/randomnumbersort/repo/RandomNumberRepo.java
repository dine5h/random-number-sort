package dinesh.code.randomnumbersort.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import dinesh.code.randomnumbersort.entity.RandomNumberEntity;

public interface RandomNumberRepo extends JpaRepository<RandomNumberEntity,Integer>{

	Optional<RandomNumberEntity> findById(Integer id);
}
